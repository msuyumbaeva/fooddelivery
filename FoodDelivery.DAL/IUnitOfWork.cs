﻿using System;
using System.Threading.Tasks;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Repositories;
using FoodDelivery.DAL.Identity;

namespace FoodDelivery.DAL
{
    public interface IUnitOfWork : IDisposable
    {
        IFoodRepository Foods { get; }
        IEstablishmentRepository Establishments { get; }
        IOrderRepository Orders { get; }
        IOrderItemRepository OrderItems { get; }
        IReviewRepository Reviews { get; }

        Task<int> CompleteAsync();
        void BeginTransaction();
        //void BeginTransaction(IsolationLevel level);
        void RollbackTransaction();
        void CommitTransaction();

        IRepository<TEntity> GetRepository<TEntity>() where TEntity : Entity;
    }
}
