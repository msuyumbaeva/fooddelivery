﻿namespace FoodDelivery.DAL.DbContext.Contracts
{
    public interface IApplicationDbContextFactory
    {
        ApplicationDbContext Create();
    }
}
