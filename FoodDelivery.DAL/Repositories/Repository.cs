﻿using FoodDelivery.Core.Models;
using FoodDelivery.Core.Repositories;
using FoodDelivery.DAL.DbContext;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodDelivery.DAL.Repositories
{
    public class Repository<T> : IRepository<T> where T : Entity
    {
        protected ApplicationDbContext _context;
        protected DbSet<T> DbSet { get; set; }

        public Repository(ApplicationDbContext context)
        {
            _context = context;
            DbSet = _context.Set<T>();
        }

        public async Task<T> AddAsync(T entity)
        {
            var entry = await DbSet.AddAsync(entity);

            return entry.Entity;
        }

        public virtual IEnumerable<T> GetAll()
        {
            return DbSet.ToList();
        }
        
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await DbSet.ToListAsync();
        }

        public virtual T GetById(int id)
        {
            return DbSet.Find(id);
        }

        public void Update(T entity)
        {
            _context.Update(entity);
        }

        public void Delete(T entity)
        {
            DbSet.Remove(entity);
        }
    }
}
