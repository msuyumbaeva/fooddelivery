﻿using FoodDelivery.Core.Models;
using FoodDelivery.Core.Repositories;
using FoodDelivery.DAL.DbContext;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoodDelivery.DAL.Repositories
{
    public class OrderRepository : Repository<Order>, IOrderRepository
    {
        public OrderRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Orders;
        }

        public override IEnumerable<Order> GetAll()
        {
            return DbSet.Include(o => o.Establishment).Include(o => o.ApplicationUser).OrderByDescending(o => o.CreatedAt).ToList();
        }

        public override Order GetById(int id)
        {
            return DbSet.Include(o => o.Establishment).Include(o => o.ApplicationUser).FirstOrDefault(o => o.Id == id);
        }
    }
}
