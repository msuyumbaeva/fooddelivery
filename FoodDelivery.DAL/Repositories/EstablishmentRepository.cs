﻿using FoodDelivery.Core.Models;
using FoodDelivery.Core.Repositories;
using FoodDelivery.DAL.DbContext;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace FoodDelivery.DAL.Repositories
{
    public class EstablishmentRepository : Repository<Establishment>, IEstablishmentRepository
    {
        public EstablishmentRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Establishments;
        }

        public override Establishment GetById(int id)
        {
            return DbSet.Include(e => e.Foods).FirstOrDefault(e => e.Id == id);
        }

        public Establishment GetByName(string name)
        {
            return DbSet.FirstOrDefault(p => p.Name == name);
        }
    }
}
