﻿using FoodDelivery.Core.Models;
using FoodDelivery.Core.Repositories;
using FoodDelivery.DAL.DbContext;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodDelivery.DAL.Repositories
{
    public class OrderItemRepository : Repository<OrderItem>, IOrderItemRepository
    {
        public OrderItemRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.OrderItems;
        }

        public Task<List<OrderItem>> GetByOrderIdAsync(int orderId)
        {
            return DbSet.Include(o=>o.Food).Where(o => o.OrderId == orderId).ToListAsync();
        }
    }
}
