﻿using FoodDelivery.Core.Models;
using FoodDelivery.Core.Repositories;
using FoodDelivery.DAL.DbContext;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodDelivery.DAL.Repositories
{
    public class ReviewRepository : Repository<Review>, IReviewRepository
    {
        public ReviewRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Reviews;
        }

        public Task<List<Review>> GetByEstablishmentIdAsync(int establishmentId)
        {
            return DbSet.Include(r => r.ApplicationUser).Where(r => r.EstablishmentId == establishmentId).ToListAsync();
        }
    }
}
