﻿using FoodDelivery.Core.Models;
using FoodDelivery.Core.Repositories;
using FoodDelivery.DAL.DbContext;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace FoodDelivery.DAL.Repositories
{
    public class FoodRepository : Repository<Food>, IFoodRepository
    {
        public FoodRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Foods;
        }

        public Food GetByNameAndEstablishmentId(string name, int establishmentId)
        {
            return DbSet.Include(f => f.Establishment).FirstOrDefault(f => f.EstablishmentId == establishmentId && f.Name == name);
        }

        public override Food GetById(int id)
        {
            return DbSet.Include(f => f.Establishment).FirstOrDefault(f => f.Id == id);
        }

        public Task<List<Food>> GetByEstablishmentId(int establishmentId)
        {
            return DbSet.Where(f => f.EstablishmentId == establishmentId).ToListAsync();
        }
    }
}
