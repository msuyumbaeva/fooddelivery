﻿using FoodDelivery.ViewModels.Food;
using System.ComponentModel.DataAnnotations;

namespace FoodDelivery.ViewModels.Order
{
    public class OrderItemCreateModel
    {
        public OrderItemCreateModel(FoodViewModel food, int quantity)
        {
            Food = food;
            Quantity = quantity;
            FoodId = food.Id;
        }
        public int FoodId { get; set; }
        [Display(Name = "Блюдо")]
        public FoodViewModel Food { get; set; }
        [Display(Name = "Кол-во")]
        public int Quantity { get; set; }
        [Display(Name = "Всего")]
        public int Sum {
            get
            {
                return Food.Price * Quantity;
            }
        }
    }
}
