﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FoodDelivery.ViewModels.Order
{
    public class OrderCreateModel         
    {
        public OrderCreateModel()
        {
            OrderItems = new List<OrderItemCreateModel>();
        }

        public int EstablishmentId { get; set; }
        public List<OrderItemCreateModel> OrderItems { get; set; }

        [Display(Name = "Итого")]
        public int TotalSum
        {
            get
            {
                int sum = 0;
                foreach (var item in OrderItems)
                    sum += item.Food.Price * item.Quantity;
                return sum;
            } 
        }
    }
}
