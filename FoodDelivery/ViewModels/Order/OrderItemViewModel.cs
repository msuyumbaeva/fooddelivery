﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodDelivery.ViewModels.Order
{
    public class OrderItemViewModel
    {
        public string FoodName { get; set; }
        public int Quantity { get; set; }
        public int FoodPrice { get; set; }
        public int Sum
        {
            get
            {
                return FoodPrice * Quantity;
            }
        }
    }
}
