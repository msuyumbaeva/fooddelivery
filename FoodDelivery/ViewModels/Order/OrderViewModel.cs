﻿using FoodDelivery.Domain.Dtos.Order;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FoodDelivery.ViewModels.Order
{
    public class OrderViewModel
    {
        public OrderViewModel()
        {
            OrderItems = new List<OrderItemViewModel>();
        }

        public int Id { get; set; }
        [Display(Name = "Дата")]
        public DateTime CreatedAt { get; set; }
        [Display(Name = "Заведение")]
        public string EstablishmentName { get; set; }
        [Display(Name = "Пользователь")]
        public string UserName { get; set; }
        [Display(Name = "Сумма заказа")]
        public int Sum { get; set; }
        [Display(Name = "Заказ")]
        public ICollection<OrderItemViewModel> OrderItems { get; set; }
    }
}
