﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FoodDelivery.ViewModels.Food
{
    public class FoodEditModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        [Display(Name = "Наименование")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Цена")]
        public int Price { get; set; }
        [Required]
        [Display(Name = "Описание")]
        public string Description { get; set; }
        public int EstablishmentId { get; set; }
    }
}
