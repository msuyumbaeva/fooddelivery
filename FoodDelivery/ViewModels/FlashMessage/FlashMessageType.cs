﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodDelivery.ViewModels.FlashMessage
{
    public enum FlashMessageType
    {
        Success,
        Warning,
        Danger
    }
}
