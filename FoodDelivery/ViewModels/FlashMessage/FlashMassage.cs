﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodDelivery.ViewModels.FlashMessage
{
    public class FlashMessage
    {
        public FlashMessage(string message, FlashMessageType type)
        {
            Message = message;
            Type = type;
        }

        public string Message { get; set; }
        public FlashMessageType Type { get; set; }
    }
}
