﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FoodDelivery.ViewModels.Establishment
{
    public class ReviewCreateModel
    {
        [Required]
        public int EstablishmentId { get; set; }
        [Required]
        [MaxLength(100)]
        public string Text { get; set; }
    }
}
