﻿using FoodDelivery.Domain.Dtos.Establishment;
using FoodDelivery.ViewModels.Food;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FoodDelivery.ViewModels.Establishment
{
    public class EstablishmentViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Наименование")]
        public string Name { get; set; }

        public string ImagePath { get; set; }

        [Display(Name = "Описание")]
        public string Description { get; set; }

        [Display(Name = "Меню")]
        public ICollection<FoodViewModel> Foods { get; set; }

        [Display(Name = "Отзывы")]
        public ICollection<ReviewDto> Reviews { get; set; }

        public int TotalSum { get; set; }

        public EstablishmentViewModel()
        {
            Foods = new List<FoodViewModel>();
            Reviews = new List<ReviewDto>();
        }
    }
}
