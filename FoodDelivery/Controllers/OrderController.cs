﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FoodDelivery.Core.Models;
using FoodDelivery.Helpers;
using FoodDelivery.ViewModels.Order;
using AutoMapper;
using FoodDelivery.Domain.Services;
using FoodDelivery.Domain.Dtos.Order;
using FoodDelivery.Domain;
using Microsoft.AspNetCore.Authorization;
using FoodDelivery.ViewModels.FlashMessage;
using FoodDelivery.ViewModels.Food;
using System.Collections.Generic;

namespace FoodDelivery.Controllers
{
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderService;
        private readonly IFoodService _foodService;
        private readonly IMapper _mapper;

        public OrderController(IOrderService orderService, IFoodService foodService, IMapper mapper)
        {
            _orderService = orderService;
            _foodService = foodService;
            _mapper = mapper;
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Index()
        {
            var result = _orderService.GetAll();
            var orders = _mapper.Map<IEnumerable<OrderViewModel>>(result.Entity);
            return View(orders);
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Details(int id)
        {
            var result = await _orderService.GetByIdAsync(id);
            var order = _mapper.Map<OrderViewModel>(result.Entity);
            return View(order);
        }

        [Authorize(Roles = "Client")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(int establishmentId)
        {
            if(ModelState.IsValid)
            {
                var orderViewModel = SessionHelper.GetObjectFromJson<OrderCreateModel>(HttpContext.Session, $"basket{establishmentId}");
                if (orderViewModel == null)
                    ModelState.AddModelError(string.Empty,"Отсутствует информация о заказе");
                var order = _mapper.Map<OrderCreateDto>(orderViewModel);
                order.UserName = User.Identity.Name;
                OperationDetails<Order> operationDetails = await _orderService.CreateAsync(order);
                if (operationDetails.IsSuccess)
                {
                    SessionHelper.RemoveFromSession(HttpContext.Session, $"basket{establishmentId}");
                    SetFlash(new FlashMessage("Заказ успешно оформлен", FlashMessageType.Success));
                    return RedirectToAction("Details", "Establishment", new { id = orderViewModel.EstablishmentId });
                }
                else
                    AddErrors(operationDetails);                   
            }
            return RedirectToAction("Basket", "Establishment", new { id = establishmentId });
        }
        
        public IActionResult AddToBasket(int id)
        {
            var result = _foodService.GetById(id);
            if (result.IsSuccess)
            {
                var food = result.Entity;
                var foodViewModel = _mapper.Map<FoodViewModel>(food);
                if (SessionHelper.GetObjectFromJson<OrderCreateModel>(HttpContext.Session, $"basket{food.EstablishmentId}") == null)
                {
                    var basket = new OrderCreateModel();
                    basket.EstablishmentId = food.EstablishmentId;
                    basket.OrderItems.Add(new OrderItemCreateModel(foodViewModel, 1));
                    SessionHelper.SetObjectAsJson(HttpContext.Session, $"basket{food.EstablishmentId}", basket);
                }
                else
                {
                    var basket = SessionHelper.GetObjectFromJson<OrderCreateModel>(HttpContext.Session, $"basket{food.EstablishmentId}");
                    int index = isExistInBasket(id);
                    if (index != -1)
                    {
                        basket.OrderItems[index].Quantity++;
                    }
                    else
                    {
                        basket.OrderItems.Add(new OrderItemCreateModel(foodViewModel, 1));
                    }
                    SessionHelper.SetObjectAsJson(HttpContext.Session, $"basket{food.EstablishmentId}", basket);
                }
                return RedirectToAction("Details", "Establishment", new { id = food.EstablishmentId });
            }
            else
                return NotFound();
        }

        public IActionResult RemoveFromBasket(int id)
        {
            var result = _foodService.GetById(id);
            if (result.IsSuccess)
            {
                var food = result.Entity;
                var basket = SessionHelper.GetObjectFromJson<OrderCreateModel>(HttpContext.Session, "basket" + food.EstablishmentId);
                var index = isExistInBasket(id);
                basket.OrderItems.RemoveAt(index);
                SessionHelper.SetObjectAsJson(HttpContext.Session, "basket" + food.EstablishmentId, basket);
                if (basket.OrderItems.Count > 0)
                    return RedirectToAction("Basket", "Establishment", new { id = food.EstablishmentId });
                else
                    return RedirectToAction("Details", "Establishment", new { id = food.EstablishmentId });
            }
            return NotFound();
        }

        private int isExistInBasket(int id)
        {
            var result = _foodService.GetById(id);
            if (result.IsSuccess)
            {
                var food = result.Entity;
                var basket = SessionHelper.GetObjectFromJson<OrderCreateModel>(HttpContext.Session, "basket" + food.EstablishmentId);
                for (int i = 0; i < basket.OrderItems.Count; i++)
                {
                    if (basket.OrderItems[i].Food.Id.Equals(id))
                    {
                        return i;
                    }
                }
                return -1;
            }
            else
                throw new Exception();
        }
    }
}