﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FoodDelivery.DAL;
using FoodDelivery.Domain.Dtos.Food;
using FoodDelivery.Domain.Services;
using FoodDelivery.ViewModels;
using FoodDelivery.ViewModels.Food;
using Microsoft.AspNetCore.Mvc;
using FoodDelivery.ViewModels.FlashMessage;
using Microsoft.AspNetCore.Authorization;

namespace FoodDelivery.Controllers
{
    public class FoodController : ControllerBase
    {
        private readonly IFoodService _foodService;
        private readonly IMapper _mapper;

        public FoodController(IFoodService foodService, IMapper mapper)
        {
            _foodService = foodService;
            _mapper = mapper;
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Create(int establishmentId)
        {
            var food = new FoodViewModel
            {
                EstablishmentId = establishmentId
            };
            return PartialView("_Create", food);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> Create(FoodViewModel foodViewModel)
        {
            if (ModelState.IsValid)
            {
                var food = _mapper.Map<FoodCreateDto>(foodViewModel);
                var result = await _foodService.CreateAsync(food);
                if (result.IsSuccess)
                {
                    SetFlash(new FlashMessage("Блюдо успешно добавлено", FlashMessageType.Success));
                }
                else
                {
                    SetFlash(new FlashMessage(result.Errors.First(), FlashMessageType.Danger));
                }
            }
            else
                SetFlash(new FlashMessage("Ошибка при заполнении формы", FlashMessageType.Danger));
            return RedirectToAction("Details", "Establishment", new { id = foodViewModel.EstablishmentId });
        }

        public async Task<IActionResult> Index(int id)
        {
            var result = await _foodService.GetByEstablishmentIdAsync(id);
            var food = new List<FoodViewModel>();
            if (result.IsSuccess)
                food = _mapper.Map<List<FoodViewModel>>(result.Entity);           
            else              
                AddErrors(result);
            return PartialView("_Index", food);
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Edit(int id)
        {
            var result = _foodService.GetById(id);
            FoodEditModel foodEditModel;
            if(result.IsSuccess)
            {
                foodEditModel = _mapper.Map<FoodEditModel>(result.Entity);
            }
            else
            {
                AddErrors(result);
                foodEditModel = new FoodEditModel();
            }
            return PartialView("_Edit",foodEditModel);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> Edit(FoodEditModel foodEditModel)
        {
            if (ModelState.IsValid)
            {
                var foodDto = _mapper.Map<FoodEditDto>(foodEditModel);
                var result = await _foodService.EditAsync(foodDto);
                if (result.IsSuccess)
                {
                    SetFlash(new FlashMessage("Блюдо успешно изменено", FlashMessageType.Success));
                }
                else
                {
                    SetFlash(new FlashMessage(result.Errors.First(), FlashMessageType.Danger));
                }
            }
            else
                SetFlash(new FlashMessage("Ошибка при заполнении формы", FlashMessageType.Danger));
            return RedirectToAction("Details", "Establishment", new { id = foodEditModel.EstablishmentId });
        }
    }
}