﻿using FoodDelivery.Domain;
using FoodDelivery.ViewModels.FlashMessage;
using Microsoft.AspNetCore.Mvc;

namespace FoodDelivery.Controllers
{
    public class ControllerBase : Controller
    {
        protected void SetFlash(FlashMessage flashMessage)
        {
            TempData["FlashMessage.Type"] = flashMessage.Type;
            TempData["FlashMessage.Text"] = flashMessage.Message;
        }

        protected void AddErrors<T>(OperationDetails<T> result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error);
            }
        }
    }
}