﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using FoodDelivery.ViewModels;
using Microsoft.AspNetCore.Authorization;

namespace FoodDelivery.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return RedirectToAction("index", "establishment");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
