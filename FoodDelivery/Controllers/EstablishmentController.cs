﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FoodDelivery.DAL;
using FoodDelivery.Domain.Dtos.Establishment;
using FoodDelivery.Domain.Services;
using FoodDelivery.Helpers;
using FoodDelivery.ViewModels.Establishment;
using FoodDelivery.ViewModels.FlashMessage;
using FoodDelivery.ViewModels.Order;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FoodDelivery.Controllers
{
    public class EstablishmentController : ControllerBase
    {
        private readonly IEstablishmentService _establishmentService;
        private readonly IMapper _mapper;

        public EstablishmentController(IEstablishmentService establishmentService, IMapper mapper)
        {
            _establishmentService = establishmentService;
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            var result = _establishmentService.GetAll();
            if (result.IsSuccess)
            {
                var viewModels = _mapper.Map<List<EstablishmentViewModel>>(result.Entity);
                return View(viewModels);
            }
            else
            {
                AddErrors(result);
                return View(new List<EstablishmentViewModel>());
            }
        }

        public async Task<IActionResult> Details(int id)
        {
            var result = await _establishmentService.GetById(id);
            if (result.IsSuccess)
            {
                var viewModel = _mapper.Map<EstablishmentViewModel>(result.Entity);
                var basket = SessionHelper.GetObjectFromJson<OrderCreateModel>(HttpContext.Session, $"basket{result.Entity.Id}");
                if (basket == null)
                    viewModel.TotalSum = 0;
                else
                    viewModel.TotalSum = basket.TotalSum;
                return View(viewModel);
            }
            else
            {
                return NotFound();
            }
        }

        [Authorize(Roles = "Client")]
        public IActionResult Basket(int id)
        {
            var viewModel = SessionHelper.GetObjectFromJson<OrderCreateModel>(HttpContext.Session, $"basket{id}");
            if (viewModel == null)
            {
                viewModel = new OrderCreateModel
                {
                    EstablishmentId = id
                };
            }
            return View(viewModel);
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            return View();
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> Create(EstablishmentCreateModel establishmentCreateModel)
        {
            var establishmentDto = _mapper.Map<EstablishmentCreateDto>(establishmentCreateModel);
            var result = await _establishmentService.CreateAsync(establishmentDto);
            if(result.IsSuccess)
            {
                SetFlash(new FlashMessage("Заведение успешно добавлено", FlashMessageType.Success));
                return RedirectToAction("Index");
            }
            else
            {
                AddErrors(result);
                return View();
            }
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int id)
        {
            var result = await _establishmentService.GetById(id);
            EstablishmentEditModel establishmentUpdateModel;
            if (result.IsSuccess)
            {
                establishmentUpdateModel = _mapper.Map<EstablishmentEditModel>(result.Entity);
            }
            else
            {
                AddErrors(result);
                establishmentUpdateModel = new EstablishmentEditModel();
            }
            return View(establishmentUpdateModel);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> Edit(EstablishmentEditModel establishmentEditModel)
        {
            var establishmentDto = _mapper.Map<EstablishmentEditDto>(establishmentEditModel);
            var result = await _establishmentService.EditAsync(establishmentDto);
            if(result.IsSuccess)
            {
                SetFlash(new FlashMessage("Заведение успешно изменено", FlashMessageType.Success));
                return RedirectToAction("Index");
            }
            else
            {
                AddErrors(result);
                return View();
            }
        }
    }
}