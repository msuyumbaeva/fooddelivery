﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FoodDelivery.Domain.Dtos.Establishment;
using FoodDelivery.Domain.Services;
using FoodDelivery.ViewModels.Establishment;
using FoodDelivery.ViewModels.FlashMessage;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FoodDelivery.Controllers
{
    public class ReviewController : ControllerBase
    {
        private readonly IReviewService _reviewService;
        private readonly IMapper _mapper;

        public ReviewController(IReviewService reviewService, IMapper mapper)
        {
            _reviewService = reviewService;
            _mapper = mapper;
        }

        public IActionResult Index()
        {

            return View();
        }

        [Authorize(Roles = "Client")]
        public IActionResult Create(int establishmentId)
        {
            var review = new ReviewCreateModel
            {
                EstablishmentId = establishmentId
            };
            return PartialView("_Create", review);
        }

        [Authorize(Roles = "Client")]
        [HttpPost]
        public async Task<IActionResult> Create(ReviewCreateModel reviewCreateModel)
        {
            if(ModelState.IsValid)
            {
                var reviewDto = _mapper.Map<ReviewDto>(reviewCreateModel);
                reviewDto.UserName = User.Identity.Name;
                var result = await _reviewService.CreateAsync(reviewDto);
                if (result.IsSuccess)
                {
                    SetFlash(new FlashMessage("Отзыв успешно добавлен", FlashMessageType.Success));
                }
                else
                {
                    SetFlash(new FlashMessage(result.Errors.First(), FlashMessageType.Danger));
                }
            }
            else
                SetFlash(new FlashMessage("Ошибка при заполнении формы", FlashMessageType.Danger));
            return RedirectToAction("Details", "Establishment", new { id = reviewCreateModel.EstablishmentId });
        }
    }
}