﻿using AutoMapper;
using FoodDelivery.Core.Models;
using FoodDelivery.Domain.Dtos.Establishment;
using FoodDelivery.Domain.Dtos.Food;
using FoodDelivery.Domain.Dtos.Order;
using FoodDelivery.ViewModels.Establishment;
using FoodDelivery.ViewModels.Food;
using FoodDelivery.ViewModels.Order;

namespace FoodDelivery
{
    class MappingProfile : Profile
    {
        public MappingProfile()
        {
            EstablishmentMapping();
            FoodMapping();
            OrderMapping();
            OrderItemMapping();
            ReviewMapping();
        }

        private void EstablishmentMapping()
        {
            CreateMap<EstablishmentDto, EstablishmentViewModel>();
            CreateMap<Establishment, EstablishmentViewModel>();
            CreateMap<EstablishmentViewModel, Establishment>();

            CreateMap<EstablishmentCreateDto, EstablishmentCreateModel>();
            CreateMap<EstablishmentCreateModel, EstablishmentCreateDto>();

            CreateMap<EstablishmentEditModel, Establishment>();
            CreateMap<Establishment, EstablishmentEditModel>();

            CreateMap<EstablishmentEditModel, EstablishmentEditDto>();
            CreateMap<EstablishmentEditDto, EstablishmentEditModel>();
        }

        private void FoodMapping()
        {
            CreateMap<Food, FoodViewModel>();
            CreateMap<FoodViewModel, Food>();

            CreateMap<FoodCreateDto, FoodViewModel>();
            CreateMap<FoodViewModel, FoodCreateDto>();

            CreateMap<Food, FoodEditModel>();
            CreateMap<FoodEditModel, Food>();

            CreateMap<FoodEditDto, FoodEditModel>();
            CreateMap<FoodEditModel, FoodEditDto>();
        }

        private void OrderMapping()
        {
            CreateMap<OrderCreateDto, OrderCreateModel>();
            CreateMap<OrderCreateModel, OrderCreateDto>();

            CreateMap<OrderDto, OrderViewModel>();
        }

        private void OrderItemMapping()
        {
            CreateMap<OrderItemCreateDto, OrderItemCreateModel>();
            CreateMap<OrderItemCreateModel, OrderItemCreateDto>();

            CreateMap<OrderItemDto, OrderItemViewModel>();
        }

        private void ReviewMapping()
        {
            CreateMap<ReviewCreateModel, ReviewDto>();
        }
    }   
}
