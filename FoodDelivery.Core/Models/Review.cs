﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodDelivery.Core.Models
{
    public class Review : Entity
    {
        public int ApplicationUserId { get; set; }
        public int EstablishmentId { get; set; }
        public string Text { get; set; }
        public DateTime CreatedAt { get; set; }

        public ApplicationUser ApplicationUser { get; set; }
    }
}
