﻿using System.Collections.Generic;

namespace FoodDelivery.Core.Models
{
    public class Establishment : Entity
    {     
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public string Description { get; set; }
        public ICollection<Food> Foods { get; set; }
        public ICollection<Review> Reviews { get; set; }

        public Establishment()
        {
            Foods = new List<Food>();
            Reviews = new List<Review>();
        }
    }
}
