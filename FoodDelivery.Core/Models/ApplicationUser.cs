﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodDelivery.Core.Models
{
    public class ApplicationUser : IdentityUser<int>
    {
    }
}
