﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodDelivery.Core.Models
{
    public class OrderItem : Entity
    {       
        public int FoodId { get; set; }
        public int FoodPrice { get; set; }
        public Food Food { get; set; }
        public int Quantity { get; set; }
        public int OrderId { get; set; }
        public Order Order { get; set; }
    }
}
