﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodDelivery.Core.Models
{
    public class Order : Entity
    {
        public Order()
        {
            OrderItems = new List<OrderItem>();
        }

        public DateTime CreatedAt { get; set; }
        public int Sum { get; set; }
        public int ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
        public int EstablishmentId { get; set; }
        public Establishment Establishment { get; set; }
        public ICollection<OrderItem> OrderItems { get; set; }
    }
}
