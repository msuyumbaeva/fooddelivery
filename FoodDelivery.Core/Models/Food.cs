﻿namespace FoodDelivery.Core.Models
{
    public class Food : Entity
    {
        public string Name { get; set; }
        public int Price { get; set; }
        public string Description { get; set; }
        public Establishment Establishment { get; set; }
        public int EstablishmentId { get; set; }
    }
}
