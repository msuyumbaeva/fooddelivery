﻿using FoodDelivery.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FoodDelivery.Core.Repositories
{
    public interface IReviewRepository : IRepository<Review>
    {
        Task<List<Review>> GetByEstablishmentIdAsync(int establishmentId);
    }
}
