﻿using FoodDelivery.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FoodDelivery.Core.Repositories
{
    public interface IRepository<T> where T : Entity
    {
        Task<T> AddAsync(T entity);
        IEnumerable<T> GetAll();
        Task<IEnumerable<T>> GetAllAsync();
        T GetById(int id);
        void Update(T entity);
        void Delete(T entity);
    }
}
