﻿using FoodDelivery.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodDelivery.Core.Repositories
{
    public interface IOrderRepository : IRepository<Order>
    {
    }
}
