﻿using FoodDelivery.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FoodDelivery.Core.Repositories
{
    public interface IFoodRepository : IRepository<Food>
    {
        Food GetByNameAndEstablishmentId(string name, int establishmentId);
        Task<List<Food>> GetByEstablishmentId(int establishmentId);
    }
}
