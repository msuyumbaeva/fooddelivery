﻿using FoodDelivery.Core.Models;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace FoodDelivery.Domain
{
    public class OperationDetails<T>
    {
        public bool IsSuccess { get; private set; }

        public T Entity { get; }

        public List<string> Errors { get; private set; }

        private OperationDetails(T entity)
        {
            Entity = entity;
            Errors = new List<string>();
        }

        private OperationDetails()
        {
            Errors = new List<string>();
        }

        public static OperationDetails<T> Success(T entity)
        {
            var result = new OperationDetails<T>(entity);
            result.IsSuccess = true;
            return result;
        }

        public static OperationDetails<T> Failure()
        {
            var result = new OperationDetails<T>();
            result.IsSuccess = false;
            result.Errors = new List<string>();

            return result;
        }

        public OperationDetails<T> AddError(params string[] errorMessages)
        {
            if (Errors == null)
            {
                Errors = new List<string>();
            }
            Errors.AddRange(errorMessages);

            return this;
        }
    }
}
