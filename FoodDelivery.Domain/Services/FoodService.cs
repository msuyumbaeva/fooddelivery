﻿using AutoMapper;
using FoodDelivery.Core.Models;
using FoodDelivery.DAL;
using FoodDelivery.Domain.Dtos.Food;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FoodDelivery.Domain.Services
{
    public interface IFoodService
    {
        OperationDetails<Food> GetById(int id);
        Task<OperationDetails<Food>> CreateAsync(FoodCreateDto foodDto);
        Task<OperationDetails<Food>> EditAsync(FoodEditDto foodDto);
        Task<OperationDetails<List<Food>>> GetByEstablishmentIdAsync(int id);
    }

    public class FoodService : IFoodService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IMapper _mapper;

        public FoodService(IUnitOfWorkFactory unitOfWorkFactory, IMapper mapper)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _mapper = mapper;
        }

        public OperationDetails<Food> GetById(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                try
                {
                    var food = unitOfWork.Foods.GetById(id);
                    return OperationDetails<Food>.Success(food);
                }
                catch (Exception ex)
                {
                    return OperationDetails<Food>.Failure().AddError(ex.Message);
                }
            }
        }

        public async Task<OperationDetails<Food>> CreateAsync(FoodCreateDto foodDto)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                try
                {
                    var food = unitOfWork.Foods.GetByNameAndEstablishmentId(foodDto.Name, foodDto.EstablishmentId);
                    if(food != null)
                        return OperationDetails<Food>
                            .Failure()
                            .AddError($"Блюдо с наименованием {food.Name} в заведении {food.Establishment.Name} уже существует");
                    food = _mapper.Map<Food>(foodDto);
                    var createdFood = await unitOfWork.Foods.AddAsync(food);
                    await unitOfWork.CompleteAsync();
                    return OperationDetails<Food>.Success(food);
                }
                catch (Exception ex)
                {
                    return OperationDetails<Food>.Failure().AddError(ex.Message);
                }
            }
        }

        public async Task<OperationDetails<List<Food>>> GetByEstablishmentIdAsync(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                try
                {
                    var food = await unitOfWork.Foods.GetByEstablishmentId(id);
                    return OperationDetails<List<Food>>.Success(food);
                }
                catch (Exception ex)
                {
                    return OperationDetails<List<Food>>.Failure().AddError(ex.Message);
                }
            }
        }

        public async Task<OperationDetails<Food>> EditAsync(FoodEditDto foodDto)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                try
                {
                    var food = unitOfWork.Foods.GetById(foodDto.Id);
                    if (food == null)
                        return OperationDetails<Food>.Failure().AddError($"Заведение с Id = {foodDto.Id} не найдено");
                    _mapper.Map(foodDto, food);
                    unitOfWork.Foods.Update(food);
                    await unitOfWork.CompleteAsync();
                    return OperationDetails<Food>.Success(food);
                }
                catch (Exception ex)
                {
                    return OperationDetails<Food>.Failure().AddError(ex.Message);
                }
            }
        }
    }
}
