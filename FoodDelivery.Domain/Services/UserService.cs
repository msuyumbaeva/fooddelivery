﻿using FoodDelivery.Core.Models;
using FoodDelivery.DAL.Identity;
using FoodDelivery.Domain.Dtos.User;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;

namespace FoodDelivery.Domain.Services
{
    public interface IUserService
    {
        Task<OperationDetails<UserDto>> CreateAsync(UserDto userDto);
        Task<IdentityResult> AuthenticateAsync(UserDto userDto);
    }

    public class UserService : IUserService
    {
        private readonly ApplicationUserManager _userManager;
        private readonly IMapper _mapper;

        public UserService(ApplicationUserManager userManager, IMapper mapper)
        {
            _userManager = userManager;
            _mapper = mapper;
        }

        public async Task<IdentityResult> AuthenticateAsync(UserDto userDto)
        {
            using (_userManager)
            {
                IdentityResult result = null;
                // находим пользователя
                ApplicationUser user = await _userManager.FindByEmailAsync(userDto.Email);
                if (user != null)
                {
                    PasswordVerificationResult validationResult = _userManager.PasswordHasher.VerifyHashedPassword(user, user.PasswordHash, userDto.Password);
                    if (validationResult == PasswordVerificationResult.Success)
                    {
                        result = await _userManager.CreateAsync(user);
                    }
                }
                return result;
            }
        }

        public async Task<OperationDetails<UserDto>> CreateAsync(UserDto userDto)
        {
            using (_userManager)
            {
                ApplicationUser user = await _userManager.FindByEmailAsync(userDto.Email);
                if (user == null)
                {
                    user = new ApplicationUser { Email = userDto.Email, UserName = userDto.Email };
                    var result = await _userManager.CreateAsync(user, userDto.Password);
                    List<IdentityError> errors = new List<IdentityError>(result.Errors);
                    if (errors.Count > 0)
                        return OperationDetails<UserDto>.Failure().AddError(errors[0].Description);
                    await _userManager.AddToRoleAsync(user, userDto.Role);
                    userDto = _mapper.Map<UserDto>(user);
                    return OperationDetails<UserDto>.Success(userDto);
                }
                else
                {
                    return OperationDetails<UserDto>.Failure().AddError("Пользователь с таким логином уже существует");
                } 
            }
        }
    }
}
