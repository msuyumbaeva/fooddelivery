﻿using FoodDelivery.DAL;
using FoodDelivery.Domain.Dtos.Establishment;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using FoodDelivery.Core.Models;
using Microsoft.AspNetCore.Http;

namespace FoodDelivery.Domain.Services
{
    public interface IEstablishmentService
    {
        OperationDetails<IEnumerable<Establishment>> GetAll();
        Task<OperationDetails<EstablishmentDto>> GetById(int id);
        Task<OperationDetails<Establishment>> CreateAsync(EstablishmentCreateDto establishmentDto);
        Task<OperationDetails<Establishment>> EditAsync(EstablishmentEditDto establishmentDto);
    }
    public class EstablishmentService : IEstablishmentService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IHostingEnvironment _hostingEnv;
        private readonly IMapper _mapper;

        private const string EstablishmentsImageFolder = "establishments";

        public EstablishmentService(IUnitOfWorkFactory unitOfWorkFactory, IHostingEnvironment hostingEnv, IMapper mapper)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _hostingEnv = hostingEnv;
            _mapper = mapper;
        }

        public async Task<OperationDetails<Establishment>> CreateAsync(EstablishmentCreateDto establishmentDto)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            { 
                try
                {
                    var establishment = unitOfWork.Establishments.GetByName(establishmentDto.Name);
                    if (establishment != null)
                        return OperationDetails<Establishment>
                            .Failure()
                            .AddError($"Заведение с наименованием {establishmentDto.Name} уже существует");               
                    UploadImageAsync(establishmentDto.ImageFile);
                    establishment = _mapper.Map<Establishment>(establishmentDto);
                    establishment.ImagePath = establishmentDto.ImageFile.FileName;
                    var createdEstablishment = await unitOfWork.Establishments.AddAsync(establishment);
                    await unitOfWork.CompleteAsync();
                    return OperationDetails<Establishment>.Success(createdEstablishment);
                }
                catch (Exception ex)
                {
                    return OperationDetails<Establishment>.Failure().AddError(ex.Message);
                }        
            }
        }

        public async Task<OperationDetails<Establishment>> EditAsync(EstablishmentEditDto establishmentDto)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                try
                {
                    var establishment = unitOfWork.Establishments.GetById(establishmentDto.Id);
                    if (establishment == null)
                        return OperationDetails<Establishment>.Failure().AddError($"Заведение с Id = {establishmentDto.Id} не найдено");
                    _mapper.Map(establishmentDto, establishment);
                    if (establishmentDto.ImageFile != null)
                    {
                        UploadImageAsync(establishmentDto.ImageFile);
                        DeleteImage(establishment.ImagePath);
                        establishment.ImagePath = establishmentDto.ImageFile.FileName;
                    }
                    unitOfWork.Establishments.Update(establishment);
                    await unitOfWork.CompleteAsync();
                    return OperationDetails<Establishment>.Success(establishment);
                }
                catch (Exception ex)
                {
                    return OperationDetails<Establishment>.Failure().AddError(ex.Message);
                }
            }            
        }

        private async void UploadImageAsync(IFormFile formFile)
        {
            var fileName = formFile.FileName.Split('.');
            fileName[0] = fileName[0] + DateTime.Now.ToBinary();
            var path = _hostingEnv.WebRootPath + @"\" + EstablishmentsImageFolder + @"\" + fileName[0] + "." + fileName[1];
            using (var stream = new FileStream(path, FileMode.Create))
            {
                await formFile.CopyToAsync(stream);
            }
        }

        private void DeleteImage(string imagePath)
        {
            var path = _hostingEnv.WebRootPath + @"\" + EstablishmentsImageFolder + @"\" + imagePath;
            File.Delete(path);
        }

        public OperationDetails<IEnumerable<Establishment>> GetAll()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                try
                {
                    var establishments = unitOfWork.Establishments.GetAll();
                    return OperationDetails<IEnumerable<Establishment>>.Success(establishments);
                }
                catch (Exception ex)
                {
                    return OperationDetails<IEnumerable<Establishment>>.Failure().AddError(ex.Message);
                }
            }
        }

        public async Task<OperationDetails<EstablishmentDto>> GetById(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                try
                {
                    var establishment = unitOfWork.Establishments.GetById(id);
                    var reviews = await unitOfWork.Reviews.GetByEstablishmentIdAsync(id);
                    establishment.Reviews = reviews;
                    var establishmentDto = _mapper.Map<EstablishmentDto>(establishment);
                    return OperationDetails<EstablishmentDto>.Success(establishmentDto);
                }
                catch (Exception ex)
                {
                    return OperationDetails<EstablishmentDto>.Failure().AddError(ex.Message);
                }
            }
        }
    }
}
