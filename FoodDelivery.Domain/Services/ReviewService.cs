﻿using AutoMapper;
using FoodDelivery.Core.Models;
using FoodDelivery.DAL;
using FoodDelivery.Domain.Dtos.Establishment;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FoodDelivery.Domain.Services
{
    public interface IReviewService
    {
        Task<OperationDetails<Review>>CreateAsync(ReviewDto reviewDto);
    }

    public class ReviewService : IReviewService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IMapper _mapper;

        public ReviewService(IUnitOfWorkFactory unitOfWorkFactory, UserManager<ApplicationUser> userManager, IMapper mapper)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _userManager = userManager;
            _mapper = mapper;
        }

        public async Task<OperationDetails<Review>> CreateAsync(ReviewDto reviewDto)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                try
                {
                    var user = await _userManager.FindByEmailAsync(reviewDto.UserName);
                    if (user == null)
                        throw new Exception($"Пользователь с эл.почтой {reviewDto.UserName} не найден");
                    var review = _mapper.Map<Review>(reviewDto);
                    review.CreatedAt = DateTime.Now;
                    review.ApplicationUserId = user.Id;
                    var createdReview = await unitOfWork.Reviews.AddAsync(review);
                    await unitOfWork.CompleteAsync();
                    return OperationDetails<Review>.Success(createdReview);
                }
                catch (Exception ex)
                {
                    return OperationDetails<Review>.Failure().AddError(ex.Message);
                }
            }
        }
    }
}
