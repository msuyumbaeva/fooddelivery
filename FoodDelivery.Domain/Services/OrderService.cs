﻿using FoodDelivery.Core.Models;
using FoodDelivery.DAL;
using FoodDelivery.Domain.Dtos.Order;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;

namespace FoodDelivery.Domain.Services
{
    public interface IOrderService
    {
        Task<OperationDetails<Order>> CreateAsync(OrderCreateDto orderDto);
        OperationDetails<IEnumerable<OrderDto>> GetAll();
        Task<OperationDetails<OrderDto>> GetByIdAsync(int id);
    }

    public class OrderService : IOrderService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IMapper _mapper;

        public OrderService(IUnitOfWorkFactory unitOfWorkFactory, UserManager<ApplicationUser> userManager, IMapper mapper)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _userManager = userManager;
            _mapper = mapper;
        }

        private int CountOrderSum(ref OrderCreateDto orderDto)
        {
            int orderSum = 0;
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                foreach (var itemDto in orderDto.OrderItems)
                {
                    var food = unitOfWork.Foods.GetById(itemDto.FoodId);
                    if (food == null)
                        throw new NullReferenceException(nameof(food));
                    itemDto.FoodPrice = food.Price;
                    orderSum += itemDto.Quantity * food.Price;
                }
            }
            return orderSum;
        }

        public async Task<OperationDetails<Order>> CreateAsync(OrderCreateDto orderDto)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                try
                {
                    unitOfWork.BeginTransaction();
                    var user = await _userManager.FindByEmailAsync(orderDto.UserName);
                    if (user == null)
                        throw new Exception($"Пользователь с эл.почтой {orderDto.UserName} не найден");
                    if (orderDto.OrderItems == null)
                        throw new Exception($"Пустой заказ пользователя {orderDto.UserName}");

                    int orderSum = CountOrderSum(ref orderDto);
                    var order = new Order();
                    order.ApplicationUserId = user.Id;
                    order.Sum = orderSum;
                    order.EstablishmentId = orderDto.EstablishmentId;
                    order.CreatedAt = DateTime.Now;
                    var createdOrder = await unitOfWork.Orders.AddAsync(order);

                    var orderItems = new List<OrderItemCreateDto>();
                    foreach (var itemDto in orderDto.OrderItems)
                    {
                        var item = _mapper.Map<OrderItem>(itemDto);
                        item.OrderId = createdOrder.Id;
                        await unitOfWork.OrderItems.AddAsync(item);
                    }

                    await unitOfWork.CompleteAsync();
                    unitOfWork.CommitTransaction();
                    return OperationDetails<Order>.Success(createdOrder);
                }
                catch (Exception ex)
                {
                    return OperationDetails<Order>.Failure().AddError(ex.Message);
                }
            } 
        }

        public OperationDetails<IEnumerable<OrderDto>> GetAll()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                try
                {
                    var orders = unitOfWork.Orders.GetAll();
                    var orderDtos = _mapper.Map<List<OrderDto>>(orders);
                    return OperationDetails<IEnumerable<OrderDto>>.Success(orderDtos);
                }
                catch (Exception ex)
                {
                    return OperationDetails<IEnumerable<OrderDto>>.Failure().AddError(ex.Message);
                }
            }
        }

        public async Task<OperationDetails<OrderDto>> GetByIdAsync(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                try
                {
                    var order = unitOfWork.Orders.GetById(id);
                    var orderDto = _mapper.Map<OrderDto>(order);
                    var orderItems = await unitOfWork.OrderItems.GetByOrderIdAsync(id);
                    orderDto.OrderItems = _mapper.Map<List<OrderItemDto>>(orderItems);
                    return OperationDetails<OrderDto>.Success(orderDto);
                }
                catch (Exception ex)
                {
                    return OperationDetails<OrderDto>.Failure().AddError(ex.Message);
                }
            }
        }
    }
}
