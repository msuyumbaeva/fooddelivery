﻿using FoodDelivery.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FoodDelivery.Domain.Dtos.Order
{
    public class OrderCreateDto
    {
        [Required]
        public string UserName { get; set; } 

        [Required]
        public int EstablishmentId { get; set; }

        [Required]
        [MinLength(1)]
        public IEnumerable<OrderItemCreateDto> OrderItems { get; set; }
    }
}
