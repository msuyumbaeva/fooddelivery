﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FoodDelivery.Domain.Dtos.Order
{
    public class OrderItemCreateDto
    {
        [Required]
        public int FoodId;

        [Required]
        public int Quantity;

        public int FoodPrice;
    }
}
