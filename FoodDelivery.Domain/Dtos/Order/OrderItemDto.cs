﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodDelivery.Domain.Dtos.Order
{
    public class OrderItemDto
    {
        public int Id { get; set; }
        public string FoodName { get; set; }
        public string FoodPrice { get; set; }
        public int Quantity { get; set; }
    }
}
