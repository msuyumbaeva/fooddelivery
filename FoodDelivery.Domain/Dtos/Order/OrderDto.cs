﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodDelivery.Domain.Dtos.Order
{
    public class OrderDto
    {
        public OrderDto()
        {
            OrderItems = new List<OrderItemDto>();
        }

        public int Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public string EstablishmentName { get; set; }
        public string UserName { get; set; }
        public int Sum { get; set; }
        public ICollection<OrderItemDto> OrderItems { get; set; }
    }
}
