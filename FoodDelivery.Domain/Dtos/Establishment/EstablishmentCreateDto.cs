﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace FoodDelivery.Domain.Dtos.Establishment
{
    public class EstablishmentCreateDto
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public IFormFile ImageFile { get; set; }
    }
}
