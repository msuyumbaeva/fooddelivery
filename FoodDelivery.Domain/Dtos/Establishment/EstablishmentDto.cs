﻿using System;
using System.Collections.Generic;
using System.Text;
using FoodDelivery.Core.Models;

namespace FoodDelivery.Domain.Dtos.Establishment
{
    public class EstablishmentDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public string Description { get; set; }
        public ICollection<FoodDelivery.Core.Models.Food> Foods { get; set; }
        public ICollection<ReviewDto> Reviews { get; set; }

        public EstablishmentDto()
        {
            Foods = new List<FoodDelivery.Core.Models.Food>();
            Reviews = new List<ReviewDto>();
        }
    }
}
