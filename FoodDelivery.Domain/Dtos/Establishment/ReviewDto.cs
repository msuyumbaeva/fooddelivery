﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FoodDelivery.Domain.Dtos.Establishment
{
    public class ReviewDto
    {
        [Required]
        public int EstablishmentId { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        [MaxLength(100)]
        public string Text { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
