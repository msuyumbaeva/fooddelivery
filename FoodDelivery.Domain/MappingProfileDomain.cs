﻿using FoodDelivery.Core.Models;
using AutoMapper;
using FoodDelivery.Domain.Dtos.Establishment;
using FoodDelivery.Domain.Dtos.Order;
using FoodDelivery.Domain.Dtos.User;
using FoodDelivery.Domain.Dtos.Food;

namespace FoodDelivery.Domain
{
    public class MappingProfileDomain : Profile
    {
        public MappingProfileDomain()
        {
            EstablishmentMapping();
            UserMapping();
            OrderMapping();
            OrderItemMapping();
            FoodMapping();
            ReviewMapping();
        }

        private void UserMapping()
        {
            CreateMap<UserDto, ApplicationUser>();
            CreateMap<ApplicationUser, UserDto>();
        }
     
        private void OrderMapping()
        {
            CreateMap<OrderCreateDto, Order>();
            CreateMap<Order, OrderCreateDto>();

            CreateMap<Order, OrderDto>()
                .ForMember(dest => dest.EstablishmentName, opt => opt.MapFrom(src => src.Establishment.Name))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.ApplicationUser.UserName));

        }

        private void OrderItemMapping()
        {
            CreateMap<OrderItem, OrderItemCreateDto>();
            CreateMap<OrderItemCreateDto, OrderItem>();

            CreateMap<OrderItem, OrderItemDto>()
                .ForMember(dest => dest.FoodName, opt => opt.MapFrom(src => src.Food.Name));
        }

        private void EstablishmentMapping()
        {
            CreateMap<Establishment, EstablishmentDto>();

            CreateMap<EstablishmentCreateDto, Establishment>();
            CreateMap<Establishment, EstablishmentCreateDto>();

            CreateMap<EstablishmentEditDto, Establishment>();
            CreateMap<Establishment, EstablishmentEditDto>();
        }

        private void FoodMapping()
        {
            CreateMap<FoodCreateDto, Food>();
            CreateMap<Food, FoodCreateDto>();

            CreateMap<FoodEditDto, Food>();
            CreateMap<Food, FoodEditDto>();
        }

        private void ReviewMapping()
        {
            CreateMap<Review, ReviewDto>()
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.ApplicationUser.UserName));
            CreateMap<ReviewDto, Review>();
        }
    }
    
}
